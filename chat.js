const ids = require('./socketIds');

const initChat = (socket) => {
	console.log("STARTED CHAT SERVER");

	socket.on(ids.NEW_MESSAGE, (msg) => {
		socket.emit(msg);
	});
};

module.exports = initChat;