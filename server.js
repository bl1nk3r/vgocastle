const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const _ = require('lodash');

const bodyParser = require("body-parser");

const passport = require('passport');
const SteamStrategy = require('passport-steam');
const session = require('express-session');

const Bot = require('./bot');
const initJackpot = require('./jackpot');
const initChat = require('./chat');

const args = {
	OP_URL: 'https://api-trade.opskins.com',
	OP_KEY: '02505e2b3880f5befee0b0b32827ca',
	OAU_KEY: 'UAVQ7ZKZJYLKUT7R',
	STEAM_URL: 'http://localhost:3000',//'http://localhost:3000',
	STEAM_KEY: '2102D59688ACEFADEB6DF43DF6F44360',
	SESSION_SECRET: 'lmao xd sdjkhgajks',
	SESSION_NAME: 'vgocastle.com',
	OFFER_CHECK_INTERVAL: 5000,
	OFFER_CHECK_TIMEOUT: 120000,
	PORT: 3001
};

const Emanuel = new Bot(args.OP_URL, args.OP_KEY, args.OAU_KEY);

app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(bodyParser.json());

passport.serializeUser(function (user, done) {
	done(null, user);
});

passport.deserializeUser(function (obj, done) {
	done(null, obj);
});

app.use(session({
    secret: args.SESSION_SECRET,
    name: args.SESSION_NAME,
    resave: true,
		saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());



passport.use(new SteamStrategy({
	returnURL: `${args.STEAM_URL}/auth/completed`,
	realm: `${args.STEAM_URL}/`,
	apiKey: args.STEAM_KEY
},
function (identifier, profile, done) {
	process.nextTick(function () {
		let user = {
			steam_id: _.last(identifier.split('/')),
			name: profile._json.personaname,
			avatar: profile._json.avatar,
		};
		profile.identifier = identifier;
		return done(null, user);
	});
}));

app.get('/auth',
	passport.authenticate('steam', { failureRedirect: '/'}),
	(req, res) => {
		res.redirect('/');
	});

app.get('/auth/completed',
	passport.authenticate('steam', { failureRedirect: '/'}),
	(req, res) => {
		res.redirect('/');
	});

app.get('/auth/status', (rq, rs) => {
	let response = rq.isAuthenticated() ? {
		authenticated: true,
		steam_id: rq.user.steam_id,
		username: rq.user.name,
		avatar: rq.user.avatar
	} : {
		authenticated: false
	};

	rs.send(response);
});

app.get('/items', (req, res) => {
	if(req.isAuthenticated()){
		Emanuel.getUserInventory(req.user.steam_id/*'76561198330739532'*/, (response) => {
			if(response.status)
				res.send(response.items);
			else
				res.send(response.error());
		});		
	} else {
		res.send('You are not logged in!');
	}
});

app.post('/deposit/jackpot', (req, res) => {
	let ids = _.map(req.body, (item) => {
		return item.id
	});
	io.emit('trade_accepted', {
		status: 1
	});
	Emanuel.sendTradeOffer(req.user.steam_id, [], ids, (response) => {
		if(response.status) {
			Emanuel.listenForOfferAccept(response.id, args.OFFER_CHECK_INTERVAL, args.OFFER_CHECK_TIMEOUT, (accepted) => {
				
				if(accepted){
					console.log('offer accepted');
					io.emit('trade_accepted', {
						status: 1
					});
				} else { 
					io.emit('trade_accepted', {
						status: 1 
					});
				}
			});
			res.send({
				status: 1
			});
		} else {
			res.send({
				status: 0
			});
		}
	});
});

io.on('connection', (socket) => {
	initJackpot(socket);
	initChat(socket);
});

http.listen(args.PORT, () => {
	console.log("SERVER IS STARTING ON PORT: " + args.PORT);
});