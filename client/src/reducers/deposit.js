import { M_TYPES } from '../actions';

export const DEFAULT_STATE = {
	state: 'none',
	items: [],
	selected_items: []
};
	
const deposit = (state = DEFAULT_STATE, { type, payload }) => {
	switch (type) {
		case M_TYPES.SET_DEPOSIT:
			return Object.assign({}, {
				...state,
				state: payload.deposit,
			});
		case M_TYPES.SET_ITEMS:
			return Object.assign({}, {
				...state,
				items: payload.items,
			});
		case M_TYPES.ADD_SELECTED_ITEM:
			return Object.assign({}, {
				...state,
				selected_items: [...state.selected_items, payload]
			});
		case M_TYPES.REMOVE_SELECTED_ITEM:
			return Object.assign({}, {
				...state,
				selected_items: state.selected_items.slice(0, payload.index)
					.concat(state.selected_items.slice(payload.index + 1))
			});
		case M_TYPES.RESET_GAMEMODE:
			return DEFAULT_STATE;
		default: 
			return state;
	}
};

export default deposit;