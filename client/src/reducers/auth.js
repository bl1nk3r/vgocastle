import { M_TYPES } from '../actions'; 

export const DEFAULT_STATE = {
	authenticated: false,
	avatar: '',
	username: ''
};

const auth = (state = DEFAULT_STATE, { type, payload }) => {
	switch(type) {
		case M_TYPES.SET_AUTH:
			return Object.assign({}, state, {
				authenticated: payload.authenticated,
				avatar: payload.avatar,
				username: payload.username
			});
		case M_TYPES.RESET_AUTH:
			return DEFAULT_STATE;
		default:
			return state;
	}	
}

export default auth;