import { combineReducers } from 'redux';

import auth from './auth';
import deposit from './deposit';
import gamemode from './gamemode';
import chat from './chat';
import jackpot from './jackpot';

export default combineReducers({
	auth,
	gamemode,
	deposit,
	chat,
	jackpot
});