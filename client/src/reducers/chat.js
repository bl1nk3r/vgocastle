import { M_TYPES } from "../actions";

const makeMessage = (avatar, name, level, text) => ({
	avatar,
	name,
	level,
	text
});

const MAX_MESSAGES = 100;

export const DEFAULT_STATE = {
	open: true,
	messages : []
};

const chat = (state = DEFAULT_STATE, { type, payload }) => {
	switch(type) {
		case M_TYPES.SET_CHAT_STATE:
			return Object.assign({}, state, {
				open: payload.open
			});
		case M_TYPES.SET_CHAT_MESSAGES:
			return Object.assign({}, state, {
				messages: payload.messages
			});
		case M_TYPES.ADD_CHAT_MESSAGE:			
			if (state.messages.length > MAX_MESSAGES) 
				return Object.assign({}, state, {
					messages: [...state.messages, makeMessage(payload.avatar, payload.name, payload.level, payload.text)].filter((elem, i) => i !== 0)
				});
			return Object.assign({}, state, {
				messages: [...state.messages, makeMessage(payload.avatar, payload.name, payload.level, payload.text)]
			});
		default:
			return state;
	}	
}

export default chat;