import { M_TYPES } from '../actions'; 

export const gamemodes = ["jackpot"];

export const DEFAULT_STATE = gamemodes[0];

const gamemode = (state = DEFAULT_STATE, { type, payload }) => {
	switch(type) {
		case M_TYPES.SET_GAMEMODE:
			return payload.name;
		default:
			return state;
	}	
}

export default gamemode;