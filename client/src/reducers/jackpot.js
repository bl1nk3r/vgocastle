import { M_TYPES } from '../actions';

const JACKPOT_MAX_TIME = 120;

const makePlayer = (name, avatar, win_precent, amount_deposited, num_of_items) => ({
	name,
	avatar,
	win_precent,
	amount_deposited,
	num_of_items
});

const DEFAULT_STATE = {
	deposit: {
		open: false	
	},
	player_list: [],
	countdown: JACKPOT_MAX_TIME 
}

const jackpot = (state = DEFAULT_STATE, { type, payload }) => {
	switch (type) {
		case M_TYPES.SET_JACKPOT_DEPOSIT_STATE:
			return Object.assign({}, state, {
				deposit: {
					open: payload.open
				}
			});
		case M_TYPES.ADD_JACKPOT_PLAYER:
			return Object.assign({}, state, {
				player_list: [...state.player_list, makePlayer(payload.name, payload.avatar, payload.win_precent, payload.amount_deposited, payload.num_of_items)]
			});
		case M_TYPES.CLEAR_JACKPOT_PLAYERS:
			return Object.assign({}, state, {
				player_list: []
			});
		case M_TYPES.SET_JACKPOT_COUNTDOWN:
			return Object.assign({}, state, {
				countdown: payload.t_left
			});
		default: 
			return state;
	}
}; 

export default jackpot;