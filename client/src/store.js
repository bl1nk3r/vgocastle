import { createStore, compose, applyMiddleware } from 'redux';
import { DEFAULT_STATE } from './reducers/auth'
import reducer from './reducers';
import thunk from 'redux-thunk';

const STARING_STATE = {
	auth: DEFAULT_STATE 
};

const enhancers = compose(
	applyMiddleware(thunk),
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const store = createStore(
	reducer,
	STARING_STATE,
	enhancers
);

export default store;