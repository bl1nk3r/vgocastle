import React, { Component } from 'react';

import { connect } from 'react-redux';
import { getAuth, setGamemode} from './actions';

class App extends Component {
	componentDidMount() {
		this.props.getAuth();
	}
	
  render() {
	let login;

	if(!this.props.auth.authenticated) 
		login = (<a href="/auth"> Log in With Steam!</a>);
	else  
		login = (<p>Hello {this.props.auth.username}</p>);
	
    return (
			<header className="App-header">
			 {login}

			</header>
    );
  }
}

export default connect( state => ({
	auth: state.auth
}),{
	getAuth,
	setGamemode,

})(App);
