const proxy = require('http-proxy-middleware');
module.exports = function (app) {
	app.use(proxy('/auth*',
		{ target: 'http://localhost:3001/' }
	));
	app.use(proxy('/auth/completed',
		{ target: 'http://localhost:3001/' }
	));
	app.use(proxy('/auth/status',
		{ target: 'http://localhost:3001/' }
	));
	app.use(proxy('/items',
		{ target: 'http://localhost:3001/' }
	));
	app.use(proxy('/deposit/jackpot',
		{ target: 'http://localhost:3001/' }
	));
	app.use(proxy('/socket.io',
		{ target: 'http://localhost:3001/' }
	));
}