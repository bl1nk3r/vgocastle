import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.js';
import './index.css';
import * as serviceWorker from './serviceWorker';

import io from 'socket.io-client';

import { Provider } from 'react-redux';
import store from './store';
import { setDeposit } from './actions';

var socket = io('http://localhost:3000', {
	resource: '/socket.io'
});

socket.on('trade_accepted', (res) => {
	if(res.status) { 
		store.dispatch(setDeposit('none'));
		console.log('Trade Accepted');
	} else { 

	}
});

const rootElement = document.getElementById('root');

ReactDOM.render(
	<Provider store={store} >
		<App />
	</Provider>,
	rootElement
);

serviceWorker.unregister();
