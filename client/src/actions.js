import axios from 'axios';

export const M_TYPES = {
	SET_AUTH: 'SET_AUTH',
	RESET_AUTH: 'RESET_AUTH',
	SET_GAMEMODE: 'SET_GAMEMODE',
	SET_DEPOSIT: 'SET_DEPOSIT',
	SET_ITEMS: 'SET_ITEMS',
	ADD_SELECTED_ITEM: 'ADD_SELECTED_ITEM',
	REMOVE_SELECTED_ITEM: 'REMOVE_SELECTED_ITEM',
	SET_CHAT_STATE: 'SET_CHAT_STATE',
	SET_CHAT_MESSAGES: 'SET_CHAT_MESSAGES',
	ADD_CHAT_MESSAGE: 'ADD_CHAT_MESSAGE',
	SET_JACKPOT_DEPOSIT_STATE: 'SET_JACKPOT_DEPOSIT_STATE',
	ADD_JACKPOT_PLAYER: 'ADD_JACKPOT_PLAYER',
	CLEAR_JACKPOT_PLAYERS: 'CLEAR_JACKPOT_PLAYERS',
	SET_JACKPOT_COUNTDOWN: 'SET_JACKPOT_COUNTDOWN'
};

const addSelectedItem = (id, name) => ({
	type: M_TYPES.ADD_SELECTED_ITEM,
	payload: {
		id,
		name
	}
});

const removeSelectedItem = (index) => ({
	type: M_TYPES.REMOVE_SELECTED_ITEM,
	payload: {
		index
	}
});

const setItems = (items) => ({
	type: M_TYPES.SET_ITEMS,
	payload: {
		items: items
	}
});

const getItems = () => {
	return dispatch => {
		axios
		  .get('/items')
		  .then(res => {
			dispatch(
			  setItems(res.data)
			);
		  })
		  .catch(error => {
			dispatch(setItems([]));
		  });
	};
};

const setGamemode = (gamemode) => ({
	type: M_TYPES.SET_GAMEMODE,
	payload: {
		name: gamemode
	}
});

const setDeposit = (state) => ({
	type: M_TYPES.SET_DEPOSIT,
	payload: {
		deposit: state
	} 
});

const setAuth = (authenticated, username, avatar) => ({
	type: M_TYPES.SET_AUTH,
	payload: {
		authenticated: authenticated,
		username: username,
		avatar: avatar
	}
});

const getAuth = () => {
	return dispatch => {
		axios
		  .get('/auth/status')
		  .then(res => {
			dispatch(
			  setAuth(res.data.authenticated, res.data.username, res.data.avatar)
			);
		  })
		  .catch(error => {
			dispatch(setAuth(false, '', ''));
		  });
	};
}; 

const setChatState = (state) => ({
	type: M_TYPES.SET_CHAT_STATE,
	payload: {
		open: state	
	}
});

const setChatMessages = (messages) => ({
	type: M_TYPES.SET_CHAT_MESSAGES,
	payload: {
		messages: messages
	}
});

const addChatMessage = (avatar, name, level, text) => ({
	type: M_TYPES.ADD_CHAT_MESSAGE,
	payload: {
		avatar,
		name,
		level,
		text
	}
});

const setJackpotDepositState = (state) => ({
	type: M_TYPES.SET_JACKPOT_DEPOSIT_STATE,
	payload: {
		open: state 
	}
});

const addJackpotPlayer = (name, avatar, win_precent, amount_deposited, num_of_items) => ({
	type: M_TYPES.ADD_JACKPOT_PLAYER,
	payload: {
		name,
		avatar,
		win_precent,
		amount_deposited,
		num_of_items
	}
});

const clearJackpotPlayers = () => ({
	type: M_TYPES.CLEAR_JACKPOT_PLAYERS
});

const setJackpotCountdown = (value) => ({
	t_left: value
});

export {
	getAuth,
	setAuth,
	setGamemode,
	setDeposit,
	getItems,
	addSelectedItem,
	removeSelectedItem,
	setChatState,
	setChatMessages,
	addChatMessage,
	setJackpotDepositState,
	addJackpotPlayer,
	clearJackpotPlayers,
	setJackpotCountdown
};