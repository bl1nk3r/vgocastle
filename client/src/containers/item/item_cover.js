import React, { Component } from 'react'

export default class ItemCover extends Component {
	render() {
		const styles = {
			width: '108px',
			height: '106.781px',
			opacity: this.props.toggled ? '0.5' : '0',
			backgroundColor: 'grey',
			position: 'absolute'
		};

		return (
			<div style={styles} />
		)
	}
}
