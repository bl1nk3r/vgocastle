import React, { Component } from 'react'

class ItemName extends Component {
	render() {
		const styles = {
			width: '100%',
			height: '22px',
			text_align: 'center',
			overflow: 'hidden',
			backgroundColor: 'red'
		};

		return (
			<div style={styles}>
				{this.props.name}
			</div>
		)
	}
}

export default ItemName;