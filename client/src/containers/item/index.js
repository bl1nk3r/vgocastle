import React, { Component } from 'react'
import { connect } from 'react-redux';

import { addSelectedItem } from '../../actions';
import { removeSelectedItem } from '../../actions';


import '../../index.css';
import ColorBar from './color_bar';
import ItemImage from './item_image';
import ItemName from './item_name';
import ItemPrice from './item_price';
import ItemCover from './item_cover';

class Item extends Component {
	constructor(props) {
		super(props);

		this.toggleItem = this.toggleItem.bind(this);
		this.state = {selected: false};
	}

/*	componentDidMount() {
		this.setState((state) => ({
			selected: false
		}));
	} */

	toggleItem(id, index, name) {
		this.setState((state) => ({
			selected: !this.state.selected
		}), () => {
			if(this.state.selected)
				this.props.addSelectedItem(id, name);
			else
				this.props.removeSelectedItem(index);
			console.log(`rxd ${id} ${index} ${this.state.selected}`);
		});
	}

  render() {
	const container_style = {
		width: '108px',
		height: '106.781px',
		margin: '4px',
		textAlign: 'center',
		backgroundColor: 'grey'
	};

	return (
	  <div className='unselectable' style={container_style} onClick={() => {this.toggleItem(this.props.id, this.props.index, this.props.name)}} >
			<ItemCover toggled={this.state.selected}/>
			<ColorBar color={this.props.color} />				
			<ItemImage url={this.props.url} />
			<ItemName name={this.props.name} />
			<ItemPrice price= {this.props.price} />
	  </div>
	)
  }
}

export default connect(() => ({}),{
	addSelectedItem,
	removeSelectedItem
})(Item);