import React, { Component } from 'react'

class ColorBar extends Component {
	render() {
		const styles = {
			backgroundColor: this.props.color,
			width: '100%',
			height: '2px'
		};

		return (
			<div style={styles} />
		)
	}
}

export default ColorBar;