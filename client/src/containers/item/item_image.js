import React, { Component } from 'react'

class ItemImage extends Component {
	render() {
		const styles = {
			backgroundImage : `url(${this.props.url})`,
			backgroundPosition: 'center',
			backgroundRepeat: 'no-repeat',
			backgroundSize: 'contain',
			objectFit: 'contain',
			width: '100%',
			height: '59px',

		};

		return (
			<div style={styles} />
		)
	}
}

export default ItemImage;