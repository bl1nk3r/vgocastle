import React, { Component } from 'react'

class ItemPrice extends Component {
	render() {
		const styles = {
			width: '100%',
			height: '24px',
			text_align: 'center',
			backgroundColor: 'blue'
		};

		return (
			<div style={styles}>
				{(parseFloat(this.props.price) / 100).toFixed(2)}$
			</div>
		)
	}
}

export default ItemPrice;