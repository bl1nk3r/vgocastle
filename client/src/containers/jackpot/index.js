import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

export class Jackpot extends Component {
	static propTypes = {
		prop: PropTypes
	}

	render() {
		const styles = {
			width: '50%',
			height: '100px',
			backgroundColor: 'grey' 
		};

		return (
			<div style={styles}>
				
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	
})

const mapDispatchToProps = {
	
}

export default connect(mapStateToProps, mapDispatchToProps)(Jackpot)
