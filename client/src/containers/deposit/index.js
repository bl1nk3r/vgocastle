import React, { Component } from 'react'
import { connect } from 'react-redux';
import '../../index.css';

import Items from './items';
import SubmitDeposit from './submit_deposit';

class DepositWindow extends Component {
	constructor(props) {
		super(props);

		this.selectItem = this.selectItem.bind(this);		
	}

	selectItem() {
		console.log('SELECTED ITEM');
	}

	render() {
		const styles = {
			display: this.props.deposit !== 'none' ? 'flex' : 'none', 
			position: 'fixed',
			zIndex: '99999',
			width: '958px',
			height: '569.65px',
			backgroundColor: '#2C3E50'
		};

		return (
				<div className='container' style={styles}>
					{ this.props.deposit === 'select_items' ? <Items items={this.props.items} /> : ''}					
					{ this.props.deposit === 'offer_sent' ? 'accept ofrer niggA' : ''}	
					<SubmitDeposit />
				</div>
		)
	}
}

export default connect(state => ({
	deposit: state.deposit.state,
	items: state.deposit.items
}))(DepositWindow);