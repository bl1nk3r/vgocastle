import React, { Component } from 'react'
import { connect } from 'react-redux';

import { setDeposit, removeSelectedItem } from '../../actions';
import axios from 'axios';

import Swal from 'sweetalert2'

class SubmitDeposit extends Component {
	constructor(props) {
		super(props);

		this.sendItems = this.sendItems.bind(this);
	}

	sendItems() {
		this.props.setDeposit('offer_sent');
		let itemsToSend;
		itemsToSend = [];

		for(var i = 0; i < this.props.selected_items.length; i++) {
			itemsToSend.push({
				id: this.props.selected_items[i].id,
				name: this.props.selected_items[i].name
			});
		}

		for(i = 0; i < this.props.selected_items.length; i++) {
			this.props.removeSelectedItem(0);
		}

		axios.post('/deposit/jackpot', itemsToSend)
		.then(function (res) {
			if(!JSON.parse(res.request.response).status)
				throw {};
		})
		.catch(function (error) {
			console.log('error :(');
			Swal({
				text: 'Bot could not send trade request.',
				type: 'error'
			});
		});
	}

	render() {
		const styles = {
			width: '100px',
			height: '50px',
			textAlign: 'center',
			backgroundColor: 'green'
		};

		return (
			<div style={styles} onClick={() => {this.sendItems()}}>Deposit</div>
		)
	}
}

export default connect((state) => ({
	selected_items: state.deposit.selected_items
}),{
	setDeposit,
	removeSelectedItem
})(SubmitDeposit);