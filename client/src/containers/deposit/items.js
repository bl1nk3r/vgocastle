import React, { Component } from 'react'
import Item from '../item';

class Items extends Component {
	render() {
		let items = [];

		for(var i = 0; i < this.props.items.length; i++) {
			let ipoint = this.props.items[i.toString()];
			items.push(<Item key={i} index={i} onClick={this.props.onClick} url={ipoint.image['300px']} color={ipoint.color} name={ipoint.name} price={ipoint.suggested_price} id={ipoint.id} />);
		}

		return (
			<>
				{items}
			</>
		)
	}
}

export default Items;