import React, { Component } from 'react'
import { connect } from 'react-redux';

import { setDeposit, getItems } from '../../actions';

class OpenDeposit extends Component {
	constructor(props) {
		super(props);

		this.setDepositState = this.setDepositState.bind(this);
	}

	setDepositState() {
		this.props.setDeposit('select_items');
		this.props.getItems();
	}

	render() {
		const styles = {
			width: '200px',
			height: '100px',
			display: this.props.auth ? 'block' : 'none',
			backgroundColor: 'blue'
		};

		return (
			<div style={styles} onClick={this.setDepositState} />
		)
	}
}

export default connect((state) => ({
	auth: state.auth.authenticated
}), {
	setDeposit,
	getItems
})(OpenDeposit);