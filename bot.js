const exec = require('child_process').exec;
const _ = require('lodash');
const axios = require('axios');

class Bot {
	constructor(opurl, opkey, oaukey){
		this.oaukey = oaukey;

		this.api = axios.create({
			baseURL: opurl,
			headers: {
				Authorization: 'Basic ' + Buffer.from(opkey + ':').toString('base64'),
			},
		});

		this.checkingOffers = false;
		this.offers = {};
	}

	getUserInventory(id, callback) {
		this.api.get('/ITrade/GetUserInventoryFromSteamId/v1/', {
			params: {
				steam_id: id,
				app_id: 1
			}
		}).then(function (response) {
			let items = _.map(response.data.response.items, (items) => {
				return {
					id: items.id,
					name: items.name,
					category: items.category,
					rarity: items.rarity,
					type: items.type,
					paint_index: items.paint_index,
					color: items.color,
					image: items.image,
					suggested_price: items.suggested_price,
					suggested_price_floor: items.suggested_price_floor,
					wear_tier_index: items.wear_tier_index,
					sku: items.sku
				};
			});
			callback({
				status: 1,
				error: '',
				items: items
			});
		}).catch(function (error) {
			callback({
				status: 0,
				error: JSON.stringify(error),
				items: []
			});
		});
	}

	sendTradeOffer(id, myItems, theirItems, callback) {
		getOAuthKey(this.oaukey, (key) => {
			this.api.post('/ITrade/SendOfferToSteamId/v1/', {
				twofactor_code: key,
				steam_id: id,
				items_to_send: JSON.stringify(myItems).split(']')[0].split('[')[1],
				items_to_receive: JSON.stringify(theirItems).split(']')[0].split('[')[1]
			}).then((res) => {
				if(res.data.status == 1)
					throw 'trade offer already sent';
				callback({
					status: 1,
					error: '',
					id: res.data.response.offer.id
				});
			}).catch((error) => {
				callback({
					status: 0,
					error: error
				});
			})
		});
	}

	getTradeOffers(callback) {
		this.api.get('/ITrade/GetOffers/v1/', {})
		.then((res) => {
			console.log(res.data.response.offers);
		})
		.catch((error) => {
			console.log(error);
		});
	}

	listenForOfferAccept(id, checkInterval = 5000, expire = 120000, callback) {
		const interval = setInterval(() => {
			this.getTradeOffer(id, (res) => {
				if(res.offer.state_name == 'Accepted') {
					callback(1);
					clearInterval(interval);
				} else if (res.offer.state_name == 'Declined') {
					callback(0);
					clearInterval(interval);
				}
			});
		}, checkInterval);
	}
	
	getTradeOffer(id, callback) {
		this.api.get(`/ITrade/GetOffer/v1/?offer_id=${id}`)
			.then((res) => {
				console.log(res.data);
				callback(res.data.response);
			})
			.catch((error) => {
				console.log(error);
			});
	}

	acceptTradeOffer(id, items, callback) {

	}
}

const getOAuthKey = (key, done) => {
	exec(`oathtool --totp -b ${key}`, function (error, stdout, stderr) {
		done(stdout);

		if (error !== null) {
			console.log('exec error: ' + error);
		}
})};

module.exports = Bot;